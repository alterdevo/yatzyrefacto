package com.alterdevo.refacto.yatzy;

import com.alterdevo.refacto.shared.Category;
import com.alterdevo.refacto.yatzy.impl.YatzyService;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class YatzyServiceTest {

    final IYatzyService yatzyService = new YatzyService();

    @Test
    public void chance() {
        Assert.assertEquals(15, yatzyService.score(Category.CHANCE, List.of(2, 3, 4, 5, 1)));
        Assert.assertEquals(16, yatzyService.score(Category.CHANCE, List.of(3, 3, 4, 5, 1)));
    }

    @Test
    public void yatzy() {
        Assert.assertEquals(50, yatzyService.score(Category.YATZY, List.of(4, 4, 4, 4, 4)));
        Assert.assertEquals(50, yatzyService.score(Category.YATZY, List.of(6, 6, 6, 6, 6)));
        Assert.assertEquals(0, yatzyService.score(Category.YATZY, List.of(6, 6, 6, 6, 3)));
    }

    @Test
    public void ones() {
        Assert.assertEquals(1, yatzyService.score(Category.ONES, List.of(1, 2, 3, 4, 5)));
        Assert.assertEquals(2, yatzyService.score(Category.ONES, List.of(1, 2, 1, 4, 5)));
        Assert.assertEquals(0, yatzyService.score(Category.ONES, List.of(6, 2, 2, 4, 5)));
        Assert.assertEquals(4, yatzyService.score(Category.ONES, List.of(1, 2, 1, 1, 1)));
    }

    @Test
    public void twos() {
        Assert.assertEquals(4, yatzyService.score(Category.TWOS, List.of(1, 2, 3, 2, 6)));
        Assert.assertEquals(10, yatzyService.score(Category.TWOS, List.of(2, 2, 2, 2, 2)));
    }

    @Test
    public void threes() {
        Assert.assertEquals(6, yatzyService.score(Category.THREES, List.of(1, 2, 3, 2, 3)));
        Assert.assertEquals(12, yatzyService.score(Category.THREES, List.of(2, 3, 3, 3, 3)));
    }

    @Test
    public void fours() {
        Assert.assertEquals(12, yatzyService.score(Category.FOURS, List.of(4, 4, 4, 5, 5)));
        Assert.assertEquals(8, yatzyService.score(Category.FOURS, List.of(4, 4, 5, 5, 5)));
        Assert.assertEquals(4, yatzyService.score(Category.FOURS, List.of(4, 5, 5, 5, 5)));
    }

    @Test
    public void fives() {
        Assert.assertEquals(10, yatzyService.score(Category.FIVES, List.of(4, 4, 4, 5, 5)));
        Assert.assertEquals(15, yatzyService.score(Category.FIVES, List.of(4, 4, 5, 5, 5)));
        Assert.assertEquals(20, yatzyService.score(Category.FIVES, List.of(4, 5, 5, 5, 5)));
    }

    @Test
    public void sixes() {
        Assert.assertEquals(0, yatzyService.score(Category.SIXES,List.of(4, 4, 4, 5, 5)));
        Assert.assertEquals(6, yatzyService.score(Category.SIXES,List.of(4, 4, 6, 5, 5)));
        Assert.assertEquals(18, yatzyService.score(Category.SIXES,List.of(6, 5, 6, 6, 5)));
    }

    @Test
    public void onePair() {
        Assert.assertEquals(6, yatzyService.score(Category.ONE_PAIR, List.of(3, 4, 3, 5, 6)));
        Assert.assertEquals(10, yatzyService.score(Category.ONE_PAIR, List.of(5, 3, 3, 3, 5)));
        Assert.assertEquals(12, yatzyService.score(Category.ONE_PAIR, List.of(5, 3, 6, 6, 5)));
    }

    @Test
    public void twoPairs() {
        Assert.assertEquals(16, yatzyService.score(Category.TWO_PAIRS, List.of(3, 3, 5, 4, 5)));
        Assert.assertEquals(16, yatzyService.score(Category.TWO_PAIRS, List.of(3, 3, 5, 5, 5)));
    }

    @Test
    public void threeOfAKind() {
        Assert.assertEquals(9, yatzyService.score(Category.THREE_OF_A_KIND, List.of(3, 3, 3, 4, 5)));
        Assert.assertEquals(15, yatzyService.score(Category.THREE_OF_A_KIND, List.of(5, 3, 5, 4, 5)));
        Assert.assertEquals(9, yatzyService.score(Category.THREE_OF_A_KIND, List.of(3, 3, 3, 3, 5)));
    }

    @Test
    public void fourOfAKind() {
        Assert.assertEquals(12, yatzyService.score(Category.FOUR_OF_A_KIND, List.of(3, 3, 3, 3, 5)));
        Assert.assertEquals(20, yatzyService.score(Category.FOUR_OF_A_KIND, List.of(5, 5, 5, 4, 5)));
        //previously a mistake was made testing three of a kind here !!!
        Assert.assertEquals(12, yatzyService.score(Category.FOUR_OF_A_KIND, List.of(3, 3, 3, 3, 3)));
    }

    @Test
    public void smallStraight() {
        Assert.assertEquals(15, yatzyService.score(Category.SMALL_STRAIGHT, List.of(1, 2, 3, 4, 5)));
        Assert.assertEquals(15, yatzyService.score(Category.SMALL_STRAIGHT, List.of(2, 3, 4, 5, 1)));
        Assert.assertEquals(0, yatzyService.score(Category.SMALL_STRAIGHT, List.of(1, 2, 2, 4, 5)));
    }

    @Test
    public void largeStraight() {
        Assert.assertEquals(20, yatzyService.score(Category.LARGE_STRAIGHT, List.of(6, 2, 3, 4, 5)));
        Assert.assertEquals(20, yatzyService.score(Category.LARGE_STRAIGHT, List.of(2, 3, 4, 5, 6)));
        Assert.assertEquals(0, yatzyService.score(Category.LARGE_STRAIGHT, List.of(1, 2, 2, 4, 5)));
    }

    @Test
    public void fullHouse() {
        Assert.assertEquals(18, yatzyService.score(Category.FULL_HOUSE,List.of(6, 2, 2, 2, 6)));
        Assert.assertEquals(0, yatzyService.score(Category.FULL_HOUSE,List.of(2, 3, 4, 5, 6)));
        Assert.assertEquals(0, yatzyService.score(Category.FULL_HOUSE,List.of(6, 2, 2, 3, 6)));
        Assert.assertEquals(0, yatzyService.score(Category.FULL_HOUSE,List.of(3, 3, 3, 3, 6)));
    }
}
