package com.alterdevo.refacto.yatzy.impl;

import com.alterdevo.refacto.roll.IDiceRollFactory;
import com.alterdevo.refacto.roll.impl.DiceRollFactory;
import com.alterdevo.refacto.shared.Category;
import com.alterdevo.refacto.shared.Die;
import com.alterdevo.refacto.yatzy.IYatzyService;

import java.util.List;
import java.util.stream.Collectors;

public class YatzyService implements IYatzyService {

    private final IDiceRollFactory diceRollFactory = new DiceRollFactory();

    @Override
    public int score(Category category, List<Integer> dice) {
        return diceRollFactory.createDiceRoll(category).rollScore(toDieList(dice));
    }


    private List<Die> toDieList(List<Integer> toConvert) {
        return toConvert.stream().map(Die::new).collect(Collectors.toList());
    }
}
