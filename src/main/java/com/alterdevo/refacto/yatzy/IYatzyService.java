package com.alterdevo.refacto.yatzy;

import com.alterdevo.refacto.shared.Category;

import java.util.List;

public interface IYatzyService {
    int score(Category category, List<Integer> dice);
}
