package com.alterdevo.refacto.roll;

import com.alterdevo.refacto.shared.Die;

import java.util.List;

public interface IDiceRoll {
    int rollScore(List<Die> dice);
}
