package com.alterdevo.refacto.roll;

import com.alterdevo.refacto.shared.Category;

public interface IDiceRollFactory {
    IDiceRoll createDiceRoll(Category category);
}
