package com.alterdevo.refacto.roll.impl;

import com.alterdevo.refacto.roll.AbstractDiceRoll;
import com.alterdevo.refacto.shared.Die;

import java.util.List;
import java.util.stream.Collectors;

public class LargeStraightRoll extends AbstractDiceRoll {
    protected int rollScoreCalculation(List<Die> dice) {
        List<Integer> listToCompare = List.of(2, 3, 4, 5, 6);
        return dice.stream().map(Die::getValue).sorted().collect(Collectors.toList()).equals(listToCompare) ? 20 : 0;
    }
}
