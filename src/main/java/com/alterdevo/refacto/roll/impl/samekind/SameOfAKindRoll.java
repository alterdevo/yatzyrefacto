package com.alterdevo.refacto.roll.impl.samekind;

import com.alterdevo.refacto.roll.AbstractDiceRoll;
import com.alterdevo.refacto.shared.Die;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SameOfAKindRoll extends AbstractDiceRoll {

    private final int occurrences;

    public SameOfAKindRoll(int occurrences) {
        this.occurrences = occurrences;
    }

    @Override
    protected int rollScoreCalculation(List<Die> dice) {
        var occurenciesMap = dice.stream().collect(Collectors.groupingBy(Die::getValue, Collectors.counting()));
        return occurenciesMap.entrySet().stream().filter(entry -> entry.getValue() >= occurrences).mapToInt(Map.Entry::getKey).sum() * occurrences;
    }

}
