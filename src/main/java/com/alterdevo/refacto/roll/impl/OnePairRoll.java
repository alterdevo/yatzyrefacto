package com.alterdevo.refacto.roll.impl;

import com.alterdevo.refacto.roll.AbstractDiceRoll;
import com.alterdevo.refacto.shared.Die;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class OnePairRoll extends AbstractDiceRoll {
    protected int rollScoreCalculation(List<Die> dice) {
        var occurencieMap = dice.stream().collect(Collectors.groupingBy(Die::getValue, Collectors.counting()));
        var maxPair = occurencieMap.entrySet().stream().filter(entry -> entry.getValue() == 2).max(Map.Entry.comparingByKey());
        return maxPair.stream().mapToInt(Map.Entry::getKey).sum() * 2;
    }
}
