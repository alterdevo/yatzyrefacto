package com.alterdevo.refacto.roll.impl.number;

public class TwosRoll extends NumberRoll {

    public TwosRoll() {
        super(2);
    }

}
