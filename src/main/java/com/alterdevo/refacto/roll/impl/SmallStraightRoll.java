package com.alterdevo.refacto.roll.impl;

import com.alterdevo.refacto.roll.AbstractDiceRoll;
import com.alterdevo.refacto.shared.Die;

import java.util.List;
import java.util.stream.Collectors;

public class SmallStraightRoll extends AbstractDiceRoll {
    protected int rollScoreCalculation(List<Die> dice) {
        List<Integer> listToCompare = List.of(1, 2, 3, 4, 5);
        return dice.stream().map(Die::getValue).sorted().collect(Collectors.toList()).equals(listToCompare) ? 15 : 0;
    }
}
