package com.alterdevo.refacto.roll.impl;

import com.alterdevo.refacto.roll.AbstractDiceRoll;
import com.alterdevo.refacto.shared.Die;

import java.util.List;

public class ChanceRoll extends AbstractDiceRoll {
    @Override
    protected int rollScoreCalculation(List<Die> dice) {
            return dice.stream().mapToInt(Die::getValue).sum();
    }
}
