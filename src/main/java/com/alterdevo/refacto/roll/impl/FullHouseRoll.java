package com.alterdevo.refacto.roll.impl;

import com.alterdevo.refacto.roll.AbstractDiceRoll;
import com.alterdevo.refacto.shared.Die;

import java.util.List;
import java.util.stream.Collectors;

public class FullHouseRoll extends AbstractDiceRoll {

    protected int rollScoreCalculation(List<Die> dice) {
        var valueOccurrencesMap = dice.stream().collect(Collectors.groupingBy(Die::getValue, Collectors.counting()));
        var threesOccurrences = valueOccurrencesMap.entrySet().stream().filter(entry -> entry.getValue() == 3).count();
        if (valueOccurrencesMap.size() == 2 && threesOccurrences == 1) {
            return valueOccurrencesMap.entrySet().stream().mapToInt(entry -> Math.toIntExact(entry.getKey() * entry.getValue())).sum();
        } else return 0;
    }

}
