package com.alterdevo.refacto.roll.impl.number;

public class OnesRoll extends NumberRoll {

    public OnesRoll() {
        super(1);
    }

}
