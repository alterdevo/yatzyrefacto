package com.alterdevo.refacto.roll.impl;

import com.alterdevo.refacto.roll.IDiceRoll;
import com.alterdevo.refacto.roll.IDiceRollFactory;
import com.alterdevo.refacto.roll.impl.number.*;
import com.alterdevo.refacto.roll.impl.samekind.FourOfAKindRoll;
import com.alterdevo.refacto.roll.impl.samekind.ThreeOfAKindRoll;
import com.alterdevo.refacto.roll.impl.samekind.TwoPairsRoll;
import com.alterdevo.refacto.shared.Category;

import java.util.EnumMap;
import java.util.Map;
import java.util.function.Supplier;

public class DiceRollFactory implements IDiceRollFactory {

    protected static Map<Category, Supplier<IDiceRoll>> diceRollMap = new EnumMap<>(Category.class);

    static {
        diceRollMap.put(Category.CHANCE, ChanceRoll::new);
        diceRollMap.put(Category.YATZY, YatzyRoll::new);
        diceRollMap.put(Category.ONES, OnesRoll::new);
        diceRollMap.put(Category.TWOS, TwosRoll::new);
        diceRollMap.put(Category.THREES, ThreesRoll::new);
        diceRollMap.put(Category.FOURS, FoursRoll::new);
        diceRollMap.put(Category.FIVES, FivesRoll::new);
        diceRollMap.put(Category.SIXES, SixesRoll::new);
        diceRollMap.put(Category.ONE_PAIR, OnePairRoll::new);
        diceRollMap.put(Category.TWO_PAIRS, TwoPairsRoll::new);
        diceRollMap.put(Category.THREE_OF_A_KIND, ThreeOfAKindRoll::new);
        diceRollMap.put(Category.FOUR_OF_A_KIND, FourOfAKindRoll::new);
        diceRollMap.put(Category.SMALL_STRAIGHT, SmallStraightRoll::new);
        diceRollMap.put(Category.LARGE_STRAIGHT, LargeStraightRoll::new);
        diceRollMap.put(Category.FULL_HOUSE, FullHouseRoll::new);
    }

    public IDiceRoll createDiceRoll(Category category) {
        Supplier<IDiceRoll> diceRoll = diceRollMap.get(category);
        if (diceRoll != null) {
            return diceRoll.get();
        } else throw new IllegalArgumentException("This category of dice roll is not available in this game !");
    }

}

