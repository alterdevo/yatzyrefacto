package com.alterdevo.refacto.roll.impl.number;

import com.alterdevo.refacto.roll.AbstractDiceRoll;
import com.alterdevo.refacto.shared.Die;

import java.util.List;

public class NumberRoll extends AbstractDiceRoll {

    private final int rollNumber;

    public NumberRoll(int rollNumber) {
        this.rollNumber = rollNumber;
    }

    @Override
    protected int rollScoreCalculation(List<Die> dice) {
        return dice.stream().filter(die -> die.getValue() == rollNumber).mapToInt(Die::getValue).sum();
    }
}
