package com.alterdevo.refacto.roll.impl;

import com.alterdevo.refacto.roll.AbstractDiceRoll;
import com.alterdevo.refacto.shared.Die;

import java.util.List;

public class YatzyRoll extends AbstractDiceRoll {
    protected int rollScoreCalculation(List<Die> dice) {
        return dice.stream().map(Die::getValue).distinct().count() <= 1 ? 50 : 0;
    }
}
