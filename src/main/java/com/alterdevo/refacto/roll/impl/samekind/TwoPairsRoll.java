package com.alterdevo.refacto.roll.impl.samekind;

public class TwoPairsRoll extends SameOfAKindRoll{

    public TwoPairsRoll() {
        super(2);
    }

}
