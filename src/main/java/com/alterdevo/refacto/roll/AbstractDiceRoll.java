package com.alterdevo.refacto.roll;

import com.alterdevo.refacto.shared.Die;

import java.util.List;

public abstract class AbstractDiceRoll implements IDiceRoll {
    @Override
    public int rollScore(List<Die> dice) {
        return rollScoreCalculation(dice);
    }

    protected abstract int rollScoreCalculation(List<Die> dice);
}
