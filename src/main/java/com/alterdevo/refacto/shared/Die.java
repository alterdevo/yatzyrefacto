package com.alterdevo.refacto.shared;

public class Die {
    private final int value;

    public Die(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
